import React from 'react';
import './App.css';
import Header from './components/Header';
import AllBlogs from './components/AllBlogs';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Register from './components/Register';
import Login from './components/Login';
import BlogPage from './components/BlogPage';

function App() {
  return (
    <div className="app">
      <Router>
        <Header />

        <Routes>
          <Route path='/' element={<AllBlogs />} />
          <Route path="/register" element={<Register />} />
          <Route path="/login" element={<Login />} />
          <Route path="/blogPage/:blogPageId" element={<BlogPage />} />
        </Routes>

      </Router>
    </div>
  );
}

export default App;

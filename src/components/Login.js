import React, { useState } from 'react';
import './styles/Login.css';
import CloseIcon from '@mui/icons-material/Close';
import { useNavigate } from 'react-router-dom';

function Login() {

    const navigate = useNavigate();
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('');

    const onFormSubmit = (e) => {
        e.preventDefault();
        console.log('Form Submitted');
    }

    return (
        <div className="login">
            <div className="login__container">
                <div className="login__header">
                    <h2>Login</h2>
                    <CloseIcon style={{ cursor: 'pointer' }} onClick={() => navigate('/')} />
                </div>

                <form className="login__form" onSubmit={onFormSubmit}>
                    <input
                        required
                        name="email"
                        type="email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        placeholder="Email" 
                    />
                    <input 
                        required
                        name="password"
                        type="password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        placeholder="Password" 
                    />

                    <button type="submit">Login</button>
                </form>

                <p className="login__forgotpw">Forgot your password?</p>
                
                <p className="login__register">Don't have an account yet? <span style={{ color: 'blue', cursor: 'pointer' }} onClick={() => navigate('/register')}>Register</span></p>
            </div>

        </div>
    )
}

export default Login;

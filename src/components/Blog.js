import React from 'react';
import './styles/Blog.css';
import { useNavigate } from "react-router-dom";

function Blog({ src, title, date, description, readTime, id }) {

    const navigate = useNavigate();

    const selectBlog = () => {
        if(id) {
            navigate(`/blogPage/${id}`)
        }
        else {
            navigate('/login')
        }
    }

    function truncate(string, n) {
        return string?.length > n ? string.substr(0, n - 1) + "..." : string;
    }

    return (
        <div className="blog">
            <img src={src} alt="" />

            <div className="blog__details">
                <h3 className="blog__title">{title}</h3>
                <p className="blog__date">{date}</p>
                <p className="blog__description">{truncate(description, 100)}</p>
            </div>

            <div className="blog__read">
                <p>{readTime}</p>
                <h4 onClick={selectBlog}>Read More</h4>
            </div>
        </div>
    )
}

export default Blog;

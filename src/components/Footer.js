import React from 'react';
import './styles/Footer.css';
import WhatsAppIcon from '@mui/icons-material/WhatsApp';
import TelegramIcon from '@mui/icons-material/Telegram';

function Footer() {
    return (
        <div className="footer">
            <div className="footer__left">
                <h2>MyWays</h2>

                <div className="footer__terms">
                    <p>Terms</p>
                    <p>Privacy Police</p>
                </div>
            </div>

            <div className="footer__right">
                <div className="footer__icons">
                    <WhatsAppIcon className="footer__icon" />
                    <TelegramIcon className="footer__icon" />
                </div>

                <p>© Copyrightcompany</p>
            </div>
        </div>
    )
}

export default Footer;

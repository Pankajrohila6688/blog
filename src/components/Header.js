import React from 'react';
import './styles/Header.css';
import { useNavigate } from 'react-router-dom';

function Header() {

    const navigate = useNavigate();

    return (
        <div className="header">

            <h2 style={{ cursor: 'pointer' }} onClick={() => navigate('/')}>MyWays</h2>

            <div className="header__btns">
                <button onClick={() => navigate('/login')}>LOG IN</button>
                <button className="active" onClick={() => navigate('/register')}>REGISTER</button>
            </div>
        </div>
    )
}

export default Header;

import React, { useState } from 'react';
import './styles/Login.css';
import CloseIcon from '@mui/icons-material/Close';
import { useNavigate } from 'react-router-dom';

function Register() {

    const navigate = useNavigate();

    const [name, setName] = useState('')
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('')
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('')

    const onFormSubmit = (e) => {
        e.preventDefault();
        console.log('Form Submitted');
    }

    return (
        <div className="login">
            <div className="login__container">
                <div className="login__header">
                    <h2>Register</h2>
                    <CloseIcon style={{ cursor: 'pointer' }} onClick={() => navigate('/')} />
                </div>

                <form className="login__form" onSubmit={onFormSubmit}>
                    <input 
                        required
                        name="name"
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                        placeholder="Full Name" 
                    />

                    <input 
                        required
                        name="email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        placeholder="Email" 
                    />

                    <input 
                        required
                        name="phone"
                        value={phone}
                        onChange={(e) => setPhone(e.target.value)}
                        placeholder="Phone Number" 
                    />

                    <input 
                        required
                        name="password"
                        value={password}
                        onChnage={(e) => setPassword(e.target.value)}
                        placeholder="Password" 
                    />

                    <input 
                        required
                        name="confirmPassword"
                        value={confirmPassword}
                        onChange={(e) => setConfirmPassword(e.target.value)}
                        placeholder="Confirm Password" 
                    />

                    <p style={{ fontSize: "15px", color: 'gray' }}>By registering, you agree to the <br /> Terms & Conditions and Privacy Policy </p>

                    <button type="submit">Register as candidate</button>
                </form>
                
                <p className="login__register">Already have an account? <span style={{ color: 'blue', cursor: 'pointer' }} onClick={() => navigate('/login')} >Login</span></p>
            </div>

        </div>
    )
}

export default Register;
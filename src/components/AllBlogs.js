import React from 'react';
import './styles/AllBlogs.css';
import Blog from './Blog';
import { blogs } from '../data';
import Footer from './Footer';


function AllBlogs() {
    return (
        <div className="allBlogs">
            <h3>MyWays Blogs</h3>

            <div style={{ display: 'flex', justifyContent: 'space-evenly' }}>
                <div className="allBlogs__content">
                    {blogs.map((blog) => (
                        <Blog 
                            id={blog.id}
                            src={blog.image}
                            title={blog.title}
                            date={blog.date}
                            description={blog.description}
                            readTime={blog.readTime}
                        />
                    ))}
                </div>
            </div>

            <Footer />
        </div>
    )
}

export default AllBlogs;

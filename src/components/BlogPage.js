import React from 'react';
import './styles/BlogPage.css';
import { useParams } from 'react-router-dom';
import { blogs } from '../data';
import { useNavigate } from "react-router-dom";

function BlogPage() {

    const { blogPageId } = useParams();
    const navigate = useNavigate();

    // useEffect(() => {

    //     if (blogPageId) {
    //         console.log(blogPageId)
    //         console.log(blogs[blogPageId]);
    //     }
    // }, [blogPageId])

    return (
        <div className="blogPage">
            <div className="blogPage__header">
                <h1>MyWays Blogs</h1>
                <p onClick={() => navigate('/')}>Go Back</p>
            </div>
            
            <div className="blogPage__content">
                <h1>{blogs[blogPageId].title}</h1>
                <img src={blogs[blogPageId].image} alt="" />
                <h5>{blogs[blogPageId].date}, {blogs[blogPageId].readTime}</h5>
                <p>{blogs[blogPageId].description}</p>
                <p>Blog description here...</p>
            </div>

        </div>
    )
}

export default BlogPage;

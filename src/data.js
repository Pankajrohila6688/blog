export const blogs = [
    {   
        id: "0",
        image: "https://images.pexels.com/photos/1342460/pexels-photo-1342460.jpeg?cs=srgb&amp;dl=pexels-vitaly-vlasov-1342460.jpg",
        title: "A Complete Guide to Start Your Career with Data Science",
        date: "Published - 2021-13-12",
        description: `Data science is a concept that binds ideas, data examination, machine learning, and other related strategies.
        Data science just simplifies complex decisions and supports decision-making from accurate predictions considered from machine learning algorithms. Modifying the behaviour and implementing smart decisions required is possible with data science.
        The field mainly includes machine learning, visualization, pattern recognition, probability model, data engineering, and signal processing.
        It involves methodically examining the large data and segregating a part of what is required and using it to develop insights to provide a personalized experience for the users in the products of varied organizations. It is directly utilized in all kinds of sectors and different fields.
        The unique usage of Data science is to have accurate predictions and solutions for the business problems that can be figured out through data science and data analysis by picking up the right information from the large data collected from all applications given by users.`,
        readTime: "16 min read",
    },
    {   
        id: "1",
        image: "https://images.unsplash.com/photo-1598015132635-131afe3ba07f?ixlib=rb-1.2.1&amp;ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&amp;auto=format&amp;fit=crop&amp;w=1170&amp;q=80.jpg",
        title: "Leadership Strategies That Steer The Team Towards Greatness",
        date: "Published - 2021-13-12",
        description: `For many leaders, the distinction between being a leader and a boss is often blurred. The autocratic relationship is deemed by them to boost the performance of the team. They want results without noticing the efforts.
        This way, often, the leaders lose the trust of their team and even indirectly hamper the organizational growth.
        Teams lose morale, and often employees leave the company because of the behaviour of their leader. 
        Sometimes this is because of the lack of exposure the leader has about heading his team.
        Well, but there is nothing that cannot be learned to change the behaviour and mindset of the leaders with these proven tips and strategies`,
        readTime: "3 min read",
    },
    {
        id: "2",
        image: "https://images.unsplash.com/photo-1573497491208-6b1acb260507?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&amp;ixlib=rb-1.2.1&amp;auto=format&amp;fit=crop&amp;w=1170&amp;q=80.jpg",
        title: "Tips For Your First Internship and your first Job",
        date: "Published - 2021-14-10",
        description: `Internships are more than just tasks that have to be ticked off on the way to university. It is about gaining experience and contacts that can assist you with your future professional goals.
        Below is a listing of suggestions on how to make the most of your tech internship and make a precise impression. Tech Interns can amplify their profits and have the ability to achieve valuable trips in any field.`,
        readTime: "3 min read",
    },
    {
        id: "3",
        image: "https://images.unsplash.com/photo-1554306274-f23873d9a26c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&amp;ixlib=rb-1.2.1&amp;auto=format&amp;fit=crop&amp;w=1500&amp;q=80.jpg",
        title: "How to learn coding for free in 2021 | 2022",
        date: "Published - 2021-01-09",
        description: `The benefit of the internet and technology is that many resources are available at the touch of a finger or click of a button. However, the disadvantage can be managing them and choosing the few right and useful resources to refer to. Hence, we have a few steps that you should follow to help organize the study material available online.`,
        readTime: "5 min read",
    },
    {
        id: "4",
        image: "https://images.unsplash.com/photo-1507209281643-9cddb381dcea?ixlib=rb-1.2.1&amp;ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&amp;auto=format&amp;fit=crop&amp;w=750&amp;q=80.jpg",
        title: "How to choose the right online course for you in 2021",
        date: "Published - 2021-01-09",
        description: `Choosing the right course can be confusing as all the academies have their advantages and disadvantages and special leverages. It is necessary to consider your expectations and requirements in your field from the course in selecting which one to study.
        For example, the purpose of a course is to add skills to your resume for areas that interest you greatly but if you already have much knowledge about it, you must consider only the courses that provide training that is more focused on the latest industry requirements and gives placement assistance as well`,
        readTime: "3 min read",
    },
    {
        id: "5",
        image: "https://images.unsplash.com/photo-1588196749597-9ff075ee6b5b?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&amp;ixlib=rb-1.2.1&amp;auto=format&amp;fit=crop&amp;w=967&amp;q=80.jpg",
        title: "15 New and Free OnlineTeam Building Games | 2021 ",
        date: "Published - 2021-30-08",
        description: `Today most of the people working in corporate are working from home. And since the pandemic days kicked in, the working environment in the corporate world hasn't been the same.
        Despite its perks, work-from-home has been causing stress and anxiety among employees.
        To foster engagement and alleviate stress, we give to the list of team-building games to connect, empathize, and understand each other, both personally and professionally.
        All these 15 games have been derived from 'Saturday Meetings' at MyWays which have been a great source for us to develop insights, learn about the company and socialize.  Moreover, it serves as a great way to take a break from work and end the week while having fun-filled evenings with our colleagues.
        If you frequently conduct such meetings or are planning to do so, there is a free bonus for you at the end.`,
        readTime: "12 min read",
    },
    {
        id: "6",
        image: "https://images.unsplash.com/photo-1558403194-611308249627?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&amp;ixlib=rb-1.2.1&amp;auto=format&amp;fit=crop&amp;w=1050&amp;q=80.jpg",
        title: "50 Words To Improve Your Startup Vocabulary",
        date: "Published - 2021-29-08",
        description: `Startups and India have a deep communication; the latter sprouts 10-20 new startups every day. 
        Today India is home to 21 Unicorns, not the one that a 12-year-old would wish to come true. A Unicorn is a startup jargon referring to companies having a valuation of over $1 billion. 
        Then, what would be a Soonicorn?
        A startup that will soon become a Unicorn, right?
        Pat yourself! 
        However, there are startup terms that don't go by their names: Angel Investors, Dragon, Cliff, ESOPs. So to avoid getting confused with startup terms, we give you a list of over 50+ startup jargon that you must know before joining a  startup or opening one.`,
        readTime: "8 min read",
    },
    {
        id: "7",
        image: "https://images.unsplash.com/photo-1508830524289-0adcbe822b40?ixlib=rb-1.2.1&amp;ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&amp;auto=format&amp;fit=crop&amp;w=711&amp;q=80.jpg",
        title: "Top 10 Online DSA Courses | List 2021 | 2022",
        date: "Published - 2021-28-08",
        description: `Data to be stored is structured and sorted appropriately so that it can be extracted, modified, and accessed for the requirement accordingly. Techies now understand the importance of learning DSA
        We have prepared a list of top 10 courses that a beginner can explore in order to get industry-based knowledge in DSA and become more employable using this skill.
        If you are a beginner or a recent graduate who is looking forward to campus placements, you must have come across many job profiles that have DSA as a required skill. Usage of heavily reused code and the need for arrays makes the future rely on DS. Efficient arrangement of data and interpreting structured form always stays in demand. To improve readable code structure DS is the right way to implement.`,
        readTime: "6 min read",
    },
    {
        id: "8",
        image: "https://images.unsplash.com/photo-1566566716921-b50e82140547?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8YXJteXxlbnwwfHwwfHw%3D&amp;ixlib=rb-1.2.1&amp;auto=format&amp;fit=crop&amp;w=500&amp;q=60.jpg",
        title: "Defence India Startup Challenge | Disc 5.0",
        date: "Published - 2021-25-08",
        description: `In order to yield technically influenced equipment and support Aatm Nirbhar Bharat in the defence technology landscape, the Defence Ministry of India launched DISC 5.0 on 19 Aug 2021 under iDEX.
        Disc 5.0 will have 35 problem statements which startups are invited to solve. Disc 1.0 which was launched in 2018  had 11 such statements and witnessed 28 winners from all across the nation.
        This challenge is followed by the announcement by the Ministry of Defence (MOD) to allocate 500 crore rupees for the next 5 years to help more than 300 startups to grow in this field.`,
        readTime: "3 min read",
    },
]